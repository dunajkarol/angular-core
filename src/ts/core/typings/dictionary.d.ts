/// <reference path="../../../../typings/tsd.d.ts" />
declare module Core {
    class Dictionary<T> implements IDictionary<T, number> {
        private _map;
        private _keys;
        private _values;
        set: (key: number, value: T) => T;
        get: (key: number, copy?: boolean) => T;
        has: (key: number) => boolean;
        keys: () => number[];
        values: () => T[];
        remove: (key: number) => T;
        clear: () => void;
    }
}
