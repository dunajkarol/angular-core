/// <reference path="../core.d.ts" />
declare module Core {
    class InMemoryCache implements ICache {
        private data;
        set: <T>(key: string, value: T) => T;
        get: <T>(key: string) => T;
        has: (key: string) => boolean;
        remove: (key: string) => void;
        clear: () => void;
        private createMap;
    }
}
