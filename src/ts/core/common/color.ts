﻿module Core {
    export class Color {
        r: number;
        g: number;
        b: number;

        constructor(r: number, g: number, b: number) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        /**
        * @param {ratio:number} zakres 0 - 1
        */
        changeValue = (ratio: number) => {
            this.r = Math.round(this.r * ratio);
            this.g = Math.round(this.g * ratio);
            this.b = Math.round(this.b * ratio);
        }

        toHex = (): string => {
            var rgb = (this.r << 16) | (this.g << 8) | (this.b);

            return '#' + rgb.toString(16);
        }

        static fromHex = (hex: string): Color => {
            var i = hex.indexOf('#');

            if (i != -1) {
                hex = hex.substr(1);
            }

            var x = parseInt(hex, 16);

            var r = (x >> 16) & 255;
            var g = (x >> 8) & 255;
            var b = x & 255;

            return new Color(r, g, b);
        }
    }
}