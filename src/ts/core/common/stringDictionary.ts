﻿/// <reference path="../../../../typings/tsd.d.ts" />

module Core {
    export class StringDictionary<T> implements IDictionary<T, string>{
        private _map: any = {};
        private _keys: string[] = [];
        private _values: T[] = [];

        set = (key: string, value: T): T => {
            var item = this._map[key];

            this._map[key] = value;

            if (item == null) {
                this._keys.push(key);

                this._values.push(value);
            } else {
                var index = this._keys.indexOf(key);

                if (index != -1) {
                    this._values[index] = value;
                }
            }

            return value;
        }

        get = (key: string, copy?: boolean): T => {
            if(copy){
                return $.extend(true, {}, this._map[key]);
            }else{
                return this._map[key]
            }
        }

        has = (key: string): boolean => {
            return this._map[key] ? true : false;
        }

        keys = (): string[] => {
            return this._keys;
        }

        values = (): T[] => {
            return this._values;
        }

        remove = (key: string): T => {
            var item = this._map[key];

            if (item == null) return;

            this._map[key] = null;

            var index = this._values.indexOf(item);

            if (index == -1) return;

            this._keys.splice(index, 1);

            return this._values.splice(index, 1)[0];
        }

        clear = (): void => {
            this._map = Object.create(null);

            this._values = [];

            this._keys = [];
        }
    }
}