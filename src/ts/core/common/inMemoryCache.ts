﻿/// <reference path="../core.d.ts" />

module Core {
    export class InMemoryCache implements ICache {
        private data = this.createMap();

        set = <T>(key: string, value: T): T => {
            this.data[key] = value;

            return value;
        }

        get = <T>(key: string): T => {
            return this.data[key];
        }

        has = (key: string): boolean => {
            return this.data[key] ? true : false;
        }

        remove = (key: string): void => {
            delete this.data[key];
        }

        clear = (): void => {
            this.data = this.createMap();
        }

        private createMap = (): any => {
            return Object.create(null);
        }
    }
}