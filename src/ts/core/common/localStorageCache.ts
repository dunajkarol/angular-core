﻿/// <reference path="../core.d.ts" />
module Core {
    export class LocalStorageCache implements ICache {
        set = <T>(key: string, value: T): T => {
            localStorage.setItem(key, JSON.stringify(value));

            return value;
        }

        get = <T>(key: string): T => {
            var value = JSON.parse(localStorage.getItem(key));

            return <T>value;
        }

        has = (key: string): boolean => {
            return localStorage.getItem(key) ? true : false;
        }

        remove = (key: string): void => {
            localStorage.removeItem(key);
        }

        clear = (): void => {
            localStorage.clear();
        }
    }
}