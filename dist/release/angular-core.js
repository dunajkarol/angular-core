/// <reference path="../../../../typings/tsd.d.ts" />
var Core;
(function (Core) {
    var Dictionary = (function () {
        function Dictionary() {
            var _this = this;
            this._map = {};
            this._keys = [];
            this._values = [];
            this.set = function (key, value) {
                var item = _this._map[key];
                _this._map[key] = value;
                if (item == null) {
                    _this._keys.push(key);
                    _this._values.push(value);
                }
                else {
                    var index = _this._keys.indexOf(key);
                    if (index != -1) {
                        _this._values[index] = value;
                    }
                }
                return value;
            };
            this.get = function (key, copy) {
                if (copy) {
                    return $.extend(true, {}, _this._map[key]);
                }
                else {
                    return _this._map[key];
                }
            };
            this.has = function (key) {
                return _this._map[key] ? true : false;
            };
            this.keys = function () {
                return _this._keys;
            };
            this.values = function () {
                return _this._values;
            };
            this.remove = function (key) {
                var item = _this._map[key];
                if (item == null)
                    return;
                _this._map[key] = null;
                var index = _this._values.indexOf(item);
                if (index == -1)
                    return;
                _this._keys.splice(index, 1);
                return _this._values.splice(index, 1)[0];
            };
            this.clear = function () {
                _this._map = Object.create(null);
                _this._values = [];
                _this._keys = [];
            };
        }
        return Dictionary;
    })();
    Core.Dictionary = Dictionary;
})(Core || (Core = {}));

/// <reference path="../core.d.ts" />

var Core;
(function (Core) {
    function Guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
    Core.Guid = Guid;
})(Core || (Core = {}));

/// <reference path="../../common.d.ts" />
var Core;
(function (Core) {
    var StorageDictionary = (function () {
        function StorageDictionary(userId, listName, entityName, deserializationFunction) {
            var _this = this;
            this._map = {};
            this._keys = [];
            this._values = [];
            this._cacheKeys = [];
            this.setIndicesToCache = function () {
                _this.localCache.set(_this.idsKey, _this._keys);
            };
            this.getIndices = function () {
                if (_this._cacheKeys.length > 0) {
                    return _this._cacheKeys;
                }
                return _this.localCache.get(_this.idsKey);
            };
            this.getKey = function (entityId) {
                return _this.idsKey + StorageDictionary.SEPARATOR + _this.enityName + StorageDictionary.SEPARATOR + entityId;
            };
            this.loadEntityFromCache = function () {
                _this._cacheKeys = _this.getIndices();
                if (_this._cacheKeys) {
                    for (var _i = 0, _a = _this._cacheKeys; _i < _a.length; _i++) {
                        var id = _a[_i];
                        var entity;
                        if (_this.deserializationFunction !== undefined && _this.deserializationFunction !== null) {
                            entity = _this.deserializationFunction(_this.localCache.get(_this.getKey(id)));
                        }
                        else {
                            entity = _this.localCache.get(_this.getKey(id));
                        }
                        _this.set(id, entity, false);
                    }
                }
                else {
                    _this._cacheKeys = [];
                }
            };
            this.set = function (key, value, addToCache) {
                if (addToCache === void 0) { addToCache = true; }
                var item = _this._map[key];
                _this._map[key] = value;
                if (item == null) {
                    _this._keys.push(key);
                    _this._values.push(value);
                }
                else {
                    var index = _this._keys.indexOf(key);
                    if (index != -1) {
                        _this._values[index] = value;
                    }
                }
                if (addToCache) {
                    _this.localCache.set(_this.getKey(key), value);
                    _this._cacheKeys.push(key);
                    _this.setIndicesToCache();
                }
                return value;
            };
            this.get = function (key) {
                return _this._map[key];
            };
            this.has = function (key) {
                return _this._map[key] ? true : false;
            };
            this.keys = function () {
                return _this._keys;
            };
            this.values = function () {
                return _this._values;
            };
            this.remove = function (key, removeFromCache) {
                if (removeFromCache === void 0) { removeFromCache = true; }
                var item = _this._map[key];
                if (item == null)
                    return;
                _this._map[key] = null;
                var index = _this._values.indexOf(item);
                if (index == -1)
                    return;
                _this._keys.splice(index, 1);
                if (removeFromCache) {
                    var cacheIndex = _this._cacheKeys.indexOf(key);
                    _this.localCache.remove(_this.getKey(key));
                    _this._cacheKeys.splice(cacheIndex, 1);
                    _this.setIndicesToCache();
                }
                return _this._values.splice(index, 1)[0];
            };
            this.clear = function () {
                _this._map = Object.create(null);
                _this._values = [];
                _this._keys = [];
                for (var index = 0, len = _this._cacheKeys.length; index < len; index++) {
                    var key = _this._cacheKeys[index];
                    _this.localCache.remove(_this.getKey(key));
                }
                _this._cacheKeys = [];
                _this.setIndicesToCache();
            };
            var instanceName = StorageDictionary.generateInstanceName(userId, listName, entityName);
            if (StorageDictionary.INSTANCES.hasOwnProperty(instanceName)) {
                throw new Error("rror: Instantiation failed: Use StorageDictionary.getInstance() instead of new.");
            }
            this.localCache = new Core.LocalStorageCache();
            this.enityName = entityName;
            this.userPrefix = StorageDictionary.STORAGENAME + StorageDictionary.SEPARATOR + userId.toString();
            this.idsKey = this.userPrefix + StorageDictionary.SEPARATOR + listName;
            this.deserializationFunction = deserializationFunction;
            this.loadEntityFromCache();
            var instanceName = StorageDictionary.generateInstanceName(userId, listName, entityName);
            StorageDictionary.INSTANCES[instanceName] = this;
        }
        StorageDictionary.SEPARATOR = "#";
        StorageDictionary.STORAGENAME = "storage";
        StorageDictionary.INSTANCES = {};
        StorageDictionary.getInstance = function (userId, listName, entityName, deserializationFunction) {
            var instanceName = StorageDictionary.generateInstanceName(userId, listName, entityName);
            if (StorageDictionary.INSTANCES.hasOwnProperty(instanceName)) {
                return StorageDictionary.INSTANCES[instanceName];
            }
            else {
                return new StorageDictionary(userId, listName, entityName, deserializationFunction);
            }
        };
        StorageDictionary.generateInstanceName = function (userId, listName, entityName) {
            return userId.toString() + "-" + listName + "-" + entityName;
        };
        return StorageDictionary;
    })();
    Core.StorageDictionary = StorageDictionary;
})(Core || (Core = {}));

/// <reference path="../../common.d.ts" />
var Core;
(function (Core) {
    var StorageDictionaryString = (function () {
        function StorageDictionaryString(userId, listName, entityName, deserializationFunction) {
            var _this = this;
            this._map = {};
            this._keys = [];
            this._values = [];
            this._cacheKeys = [];
            this.setIndicesToCache = function () {
                _this.localCache.set(_this.idsKey, _this._keys);
            };
            this.getIndices = function () {
                if (_this._cacheKeys.length > 0) {
                    return _this._cacheKeys;
                }
                return _this.localCache.get(_this.idsKey);
            };
            this.getKey = function (entityId) {
                return _this.idsKey + Core.StorageDictionary.SEPARATOR + _this.enityName + Core.StorageDictionary.SEPARATOR + entityId;
            };
            this.loadEntityFromCache = function () {
                _this._cacheKeys = _this.getIndices();
                if (_this._cacheKeys) {
                    for (var _i = 0, _a = _this._cacheKeys; _i < _a.length; _i++) {
                        var id = _a[_i];
                        var entity;
                        if (_this.deserializationFunction !== undefined && _this.deserializationFunction !== null) {
                            entity = _this.deserializationFunction(_this.localCache.get(_this.getKey(id)));
                        }
                        else {
                            entity = _this.localCache.get(_this.getKey(id));
                        }
                        _this.set(id, entity, false);
                    }
                }
                else {
                    _this._cacheKeys = [];
                }
            };
            this.set = function (key, value, addToCache) {
                if (addToCache === void 0) { addToCache = true; }
                var item = _this._map[key];
                _this._map[key] = value;
                if (item == null) {
                    _this._keys.push(key);
                    _this._values.push(value);
                }
                else {
                    var index = _this._keys.indexOf(key);
                    if (index != -1) {
                        _this._values[index] = value;
                    }
                }
                if (addToCache) {
                    _this.localCache.set(_this.getKey(key), value);
                    _this._cacheKeys.push(key);
                    _this.setIndicesToCache();
                }
                return value;
            };
            this.get = function (key) {
                return _this._map[key];
            };
            this.has = function (key) {
                return _this._map[key] ? true : false;
            };
            this.keys = function () {
                return _this._keys;
            };
            this.values = function () {
                return _this._values;
            };
            this.remove = function (key, removeFromCache) {
                if (removeFromCache === void 0) { removeFromCache = true; }
                var item = _this._map[key];
                if (item == null)
                    return;
                _this._map[key] = null;
                var index = _this._values.indexOf(item);
                if (index == -1)
                    return;
                _this._keys.splice(index, 1);
                if (removeFromCache) {
                    var cacheIndex = _this._cacheKeys.indexOf(key);
                    _this.localCache.remove(_this.getKey(key));
                    _this._cacheKeys.splice(cacheIndex, 1);
                    _this.setIndicesToCache();
                }
                return _this._values.splice(index, 1)[0];
            };
            this.clear = function () {
                _this._map = Object.create(null);
                _this._values = [];
                _this._keys = [];
                for (var index = 0, len = _this._cacheKeys.length; index < len; index++) {
                    var key = _this._cacheKeys[index];
                    _this.localCache.remove(_this.getKey(key));
                }
                _this._cacheKeys = [];
                _this.setIndicesToCache();
            };
            var instanceName = Core.StorageDictionary.generateInstanceName(userId, listName, entityName);
            if (Core.StorageDictionary.INSTANCES.hasOwnProperty(instanceName)) {
                throw new Error("rror: Instantiation failed: Use StorageDictionary.getInstance() instead of new.");
            }
            this.localCache = new Core.LocalStorageCache();
            this.enityName = entityName;
            this.userPrefix = Core.StorageDictionary.STORAGENAME + Core.StorageDictionary.SEPARATOR + userId.toString();
            this.idsKey = this.userPrefix + Core.StorageDictionary.SEPARATOR + listName;
            this.deserializationFunction = deserializationFunction;
            this.loadEntityFromCache();
            var instanceName = Core.StorageDictionary.generateInstanceName(userId, listName, entityName);
            StorageDictionaryString.INSTANCES[instanceName] = this;
        }
        StorageDictionaryString.SEPARATOR = "#";
        StorageDictionaryString.STORAGENAME = "storage";
        StorageDictionaryString.INSTANCES = {};
        StorageDictionaryString.getInstance = function (userId, listName, entityName, deserializationFunction) {
            var instanceName = Core.StorageDictionary.generateInstanceName(userId, listName, entityName);
            if (Core.StorageDictionary.INSTANCES.hasOwnProperty(instanceName)) {
                return StorageDictionaryString.INSTANCES[instanceName];
            }
            else {
                return new StorageDictionaryString(userId, listName, entityName, deserializationFunction);
            }
        };
        StorageDictionaryString.generateInstanceName = function (userId, listName, entityName) {
            return userId.toString() + "-" + listName + "-" + entityName;
        };
        return StorageDictionaryString;
    })();
    Core.StorageDictionaryString = StorageDictionaryString;
})(Core || (Core = {}));

/// <reference path="../../../../typings/tsd.d.ts" />
var Core;
(function (Core) {
    var StringDictionary = (function () {
        function StringDictionary() {
            var _this = this;
            this._map = {};
            this._keys = [];
            this._values = [];
            this.set = function (key, value) {
                var item = _this._map[key];
                _this._map[key] = value;
                if (item == null) {
                    _this._keys.push(key);
                    _this._values.push(value);
                }
                else {
                    var index = _this._keys.indexOf(key);
                    if (index != -1) {
                        _this._values[index] = value;
                    }
                }
                return value;
            };
            this.get = function (key, copy) {
                if (copy) {
                    return $.extend(true, {}, _this._map[key]);
                }
                else {
                    return _this._map[key];
                }
            };
            this.has = function (key) {
                return _this._map[key] ? true : false;
            };
            this.keys = function () {
                return _this._keys;
            };
            this.values = function () {
                return _this._values;
            };
            this.remove = function (key) {
                var item = _this._map[key];
                if (item == null)
                    return;
                _this._map[key] = null;
                var index = _this._values.indexOf(item);
                if (index == -1)
                    return;
                _this._keys.splice(index, 1);
                return _this._values.splice(index, 1)[0];
            };
            this.clear = function () {
                _this._map = Object.create(null);
                _this._values = [];
                _this._keys = [];
            };
        }
        return StringDictionary;
    })();
    Core.StringDictionary = StringDictionary;
})(Core || (Core = {}));

/// <reference path="../core.d.ts" />
var Core;
(function (Core) {
    var InMemoryCache = (function () {
        function InMemoryCache() {
            var _this = this;
            this.data = this.createMap();
            this.set = function (key, value) {
                _this.data[key] = value;
                return value;
            };
            this.get = function (key) {
                return _this.data[key];
            };
            this.has = function (key) {
                return _this.data[key] ? true : false;
            };
            this.remove = function (key) {
                delete _this.data[key];
            };
            this.clear = function () {
                _this.data = _this.createMap();
            };
            this.createMap = function () {
                return Object.create(null);
            };
        }
        return InMemoryCache;
    })();
    Core.InMemoryCache = InMemoryCache;
})(Core || (Core = {}));

/// <reference path="../core.d.ts" />
var Core;
(function (Core) {
    var LocalStorageCache = (function () {
        function LocalStorageCache() {
            this.set = function (key, value) {
                localStorage.setItem(key, JSON.stringify(value));
                return value;
            };
            this.get = function (key) {
                var value = JSON.parse(localStorage.getItem(key));
                return value;
            };
            this.has = function (key) {
                return localStorage.getItem(key) ? true : false;
            };
            this.remove = function (key) {
                localStorage.removeItem(key);
            };
            this.clear = function () {
                localStorage.clear();
            };
        }
        return LocalStorageCache;
    })();
    Core.LocalStorageCache = LocalStorageCache;
})(Core || (Core = {}));

/// <reference path="../common.d.ts" />
var Core;
(function (Core) {
    angular.module('Core', []);
})(Core || (Core = {}));
